//1-----------
const company = {
  company: null,
  country: null,
  guarantee: null,
  getGuarantee: function(furniture) {
    return this.guarantee >= furniture.age;
  },
};
const BBF = {
  company: 'BBF',
  country: 'Russia',
  guarantee: 2,
  __proto__: company,
};

const BCF = {
  company: 'BCF',
  country: 'Russia',
  guarantee: 4,
  __proto__: company,
};

const furniture = {
  company: null,
  type: null,
  age: null,
  isNew: false,
  refund: function () {
    if (this.isNew) {
      console.log('нечего менять, все и так новое');
    } else {
      const resultGetGuarantee = this.company?.getGuarantee(this);//помогли,  если не будет company
      if (resultGetGuarantee) {
        this.isNew = true;
        console.log(`${this.type} принадлежащий компании ${this.company.company} заменен на новый`);
      } else {
        console.log('мебель уже слишком старая, не выйдет вернуть');
      }
    }
  }
};

const furnitureBBFOne = {
  company: BBF,
  type: 'stool',
  age: 5,
  __proto__: furniture,
};

const furnitureBBFTwo = {
  company: BBF,
  type: 'stool',
  age: 1,
  __proto__: furniture,
};

const furnitureBCFOne = {
  company: BCF,
  type: 'stool',
  age: 3,
  __proto__: furniture,
};

const furnitureBCFTwo = {
  company: BCF,
  type: 'table',
  age: 1,
  __proto__: furniture,
};

furnitureBBFOne.refund();
furnitureBBFTwo.refund();
furnitureBBFTwo.refund();
furnitureBCFOne.refund();
furnitureBCFTwo.refund();

//3--------------

class Barn {
  constructor(count, maxCount) {
    this.count = count;
    this.maxCount = maxCount;
  }

  isOverflow = false;//помогли
}

const oneBarn = new Barn(5, 20);

class Worker {
  constructor(name, strong) {
    this.name = name;
    this.strong = strong;
  }

  isKing = false;

  add(barn) {
    if (!barn.isOverflow) {
      barn.count = barn.count + this.strong;

      if (barn.maxCount <= barn.count) {
        this.isKing = true;
        barn.isOverflow = true;

        console.log('амбар полон');
        console.log(`${this.name} is King`);
      } 
    } else {
      console.log('все, амбар полон');
    }
  }
}

const workerDima = new Worker('Dima', 7);
const workerSasha = new Worker('Sasha', 5);

workerDima.add(oneBarn);
workerDima.add(oneBarn);
workerSasha.add(oneBarn);
workerSasha.add(oneBarn);

  
//2-------------
const objTwo = {//помогли
  count: 0,
  add: function() {
    this.count = this.count + 1;
  },
  reduce: function() {
    this.count = this.count - 1;
  },
  init: function(number) {
    this.count =  number;
  },
  showinfo: function() {
    console.log(`count ${this.count}`);
  }
}

objTwo.add();
objTwo.init(2);
objTwo.reduce();
objTwo.showinfo();
console.log(objTwo.count);